import logging

from retailiate_core.core import Retailiate
from starlette.requests import Request
from utils import Context

#from prefect import task

async def handle(request: Request, retailiate: Retailiate, context: Context):
    """handle a request to the function
    Args:
        req: request body
    """

    return request
